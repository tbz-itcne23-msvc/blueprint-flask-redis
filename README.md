# Microservices - Asynchrone Kommunikation

## Technologien

- Flask (https://flask.palletsprojects.com/en/3.0.x/)
- ApiFlask (https://apiflask.com/)
- SQLAlchemy (https://www.sqlalchemy.org/)
- mySQL (https://www.mysql.com/de/)
- Docker Compose (https://docs.docker.com/compose/)
- pyTest (https://docs.pytest.org/en/8.0.x/)
- Gunicorn (https://gunicorn.org/)
- Redis (https://redis.io/docs/latest/develop/interact/pubsub/)

## Zweck

Dies ist eine Beispielapplikation für einen dockerized Flask-Server mit einer Dev und einer Prod Variante.

In dieser Variante bauen demonstrieren wir asynchrone Kommunikation mithilfe von redis pub/sub.


## Funktionen

- Anmelden mit Name
- Nachricht in den chat senden

## Design

```plantuml

class Message {
    message : String
    sent_at : DateTime
}

hide empty members
hide circle

```

## Installation

klone dieses Repo und wechsle in das Verzeichnis mit der Datei compose.yaml

Development Env (mit Hot Reload):

```bash
docker compose up --build
```

Tests ausführen:

```bash
docker compose -f compose.test.yaml up --build
```

Produktion:

```bash
docker compose -f compose.prod.yaml up --build
```

## CI/CD

Eine Konfiguration für GitLab-CI ist im Projekt angelegt. Damit die Pipeline funktioniert müssen folgende Variablen bei den Projekt- oder Gruppen-CI Variablen gesetzt werden:

- DEPLOY_TARGET - die IP-Adresse oder der DNS-Name des Ziel-Servers
- SSH_HOST_KEY - generiert durch _sudo ssh-keygen -l -f ~/.ssh/authorized_keys_ auf dem server (zum hinzufügen des Hosts zu den vertrauensvollen Servern ohne Rückfrage.)
- SSH_PRIVATE_KEY - der private SSH-Key des Servers (auf AWS EC2 normalerweise während der Erstellung generiert)


## Lizenz

© 2024. This work is openly licensed via [CC BY-NC.SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).
