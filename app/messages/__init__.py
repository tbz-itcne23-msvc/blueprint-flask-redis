from apiflask import APIBlueprint

bp = APIBlueprint('messages', __name__)

from app.messages import routes