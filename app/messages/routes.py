from app.messages import bp
from app.extensions import db
from app.models.message import Message, MessageOut


####
## view functions
####    
@bp.get('/<int:message_id>')
@bp.output(MessageOut)
def get_message(message_id):
    return db.get_or_404(Message, message_id)

@bp.get('/')
@bp.output(MessageOut(many=True))
def get_messages():
    return Message.query.all()