from apiflask import Schema
from apiflask.fields import Integer, String
from apiflask.validators import Length, OneOf

from app.extensions import db

   
class MessageOut(Schema):
    id = Integer()
    title = String()

class Message(db.Model):
    __tablename__ = 'courses'
    id = db.Column(db.Integer, primary_key=True)
    message = db.Column(db.String(512))
    sent_at = db.Column(db.Date)